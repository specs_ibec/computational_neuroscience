#!/bin/bash

# Upgrade system
#sudo apt update
#DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical sudo apt -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade

# Install pip3
DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical sudo apt install -y python3-pip

# Install python3 libraries
pip install --user numpy scipy
