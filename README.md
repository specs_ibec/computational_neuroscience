Use this repository to have a public link to the post-install script of your AWS instances. 

## Using cfncluster

Add the following line under [cluster default]:
post_install = https://bitbucket.org/specs_ibec/computational_neuroscience/raw/4c54539950456504663076a68be43ae122ec932a/post_install.sh