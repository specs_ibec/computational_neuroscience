#!/bin/bash

# Upgrade system
sudo apt update
DEBIAN_FRONTEND=noninteractive DEBIAN_PRIORITY=critical sudo apt -q -y -o "Dpkg::Options::=--force-confdef" -o "Dpkg::Options::=--force-confold" upgrade

# Install anaconda
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
bash /tmp/miniconda.sh -b -p /opt/miniconda
export PATH="/opt/miniconda/bin:$PATH"
echo "export PATH=\"/opt/miniconda/bin:$PATH\"" >> ~/.bashrc

# Install most used libraries
conda config --add channels intel
conda upgrade -y --all
conda install -y intelpython3_full
